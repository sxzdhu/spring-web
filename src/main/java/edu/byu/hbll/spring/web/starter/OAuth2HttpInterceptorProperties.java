package edu.byu.hbll.spring.web.starter;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("byuhbll.spring-web.oauth2")
@Data
public class OAuth2HttpInterceptorProperties {

  /** The OAuth2 clientId. */
  private String clientId;

  /** The OAuth2 clientSecret. */
  private String clientSecret;

  /** The OAuth2 access_token endpoint. */
  private String tokenUri;

  /** The number of times to attempt to obtain a token before giving up. */
  private int loginAttemptsAllowed;
}
