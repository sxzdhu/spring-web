package edu.byu.hbll.spring.web.starter;

import edu.byu.hbll.spring.web.oauth2.OAuth2HttpInterceptor;
import edu.byu.hbll.spring.web.oauth2.OAuth2RestTemplate;
import lombok.AllArgsConstructor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/** Autoconfiguration for {@link OAuth2RestTemplate}. */
@Configuration
@ConditionalOnBean(OAuth2HttpInterceptor.class)
@AllArgsConstructor
public class OAuth2RestTemplateAutoConfiguration {

  private OAuth2HttpInterceptor interceptor;

  @Bean
  @ConditionalOnMissingBean
  public OAuth2RestTemplate oauth2RestTemplate() {
    return new OAuth2RestTemplate(interceptor);
  }
}
