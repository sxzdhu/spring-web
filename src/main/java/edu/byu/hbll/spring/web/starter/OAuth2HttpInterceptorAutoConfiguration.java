package edu.byu.hbll.spring.web.starter;

import edu.byu.hbll.spring.web.oauth2.OAuth2HttpInterceptor;
import java.net.URI;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/** Autoconfiguration for {@link OAuth2HttpInterceptor}. */
@Configuration
@AutoConfigureBefore(OAuth2RestTemplateAutoConfiguration.class)
@ConditionalOnMissingBean(OAuth2HttpInterceptor.class)
@ConditionalOnProperty(
    prefix = "byuhbll.spring-web.oauth2",
    name = {"client-id", "client-secret", "token-uri"})
@EnableConfigurationProperties(OAuth2HttpInterceptorProperties.class)
@AllArgsConstructor
@Slf4j
public class OAuth2HttpInterceptorAutoConfiguration {

  private OAuth2HttpInterceptorProperties properties;

  @Bean
  public OAuth2HttpInterceptor oauth2HttpInterceptor() {
    log.info("configuring OAuth2HttpInterceptor");

    return OAuth2HttpInterceptor.builder()
        .clientId(properties.getClientId())
        .clientSecret(properties.getClientSecret())
        .tokenUri(URI.create(properties.getTokenUri()))
        .loginAttemptsAllowed(properties.getLoginAttemptsAllowed())
        .build();
  }
}
