# Spring Web Utilities

### OAuth2HttpInterceptor

The `OAuth2HttpInterceptor` allows you to seamlessly access resources that have been protected with
OAuth2 using the client credentials flow. In other words, it facilitates one api accessing another
using OAuth2.

If your application only needs to access one OAuth2 server with one set of credentials, you can use
spring boot autoconfiguration:

```yaml
byuhbll:
  spring-web:
    oauth2:
      token-uri: https://example.com/token
      client-id: myClientId
      client-secret: myClientSecret
      login-attempts-allowed: 1 # optional. default: 3
```

```java
@Autowired OAuth2HttpInterceptor interceptor;
```

If you need to construct multiple `OAuth2HttpInterceptor`s or you're not using spring boot, the
following is an example of creating a new instance:

```java
OAuth2HttpInterceptor interceptor = OAuth2HttpInterceptor.builder()
    .tokenUri(URI.create("https://example.com/token"))
    .clientId("myClientId")
    .clientSecret("myClientSecret")
    .loginAttemptsAllowed(1)
    .build();
```

### OAuth2RestTemplate

The `OAuth2RestTemplate` is a `RestTemplate` implementation that requires an
`OAuth2HttpInterceptor`.

If your application uses Spring Boot Autoconfiguration, simply provide an `OAuth2HttpInterceptor`
as a bean either by using the above autoconfiguration method or by registering your own. If an
`OAuth2HttpInterceptor` bean is present, an `OAuth2RestTemplate` will automatically be configured
with that bean and will be available for injection.

```java
@Autowired OAuth2RestTemplate restTemplate;
```

If you need more than one, it can be constructed as follows:

```java
OAuth2HttpInterceptor interceptor = ...
OAuth2RestTemplate restTemplate = new OAuth2RestTemplate(interceptor);
```
